<%--
  Created by IntelliJ IDEA.
  User: ASUS TUF
  Date: 3/29/2023
  Time: 11:33 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>LogIn</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css"/>
</head>
<body>
<div class="container-fluid ">
    <div class="row align-content-center ">

        <div class="col-6 offset-3 bg-white">

        <h1 class="text-center h3-class fs-1">LOGIN</h1>


        <form action="login" method="POST">


            <div class="row g-2">
                <div class="col-12 ">
                    <label class="form-label fs-4 text-black-50">First Name</label>
                    <input class="form-control fs-5 text-dark" type="text" name="fname"/>
                </div>
            </div>

            <br/><br/>

            <div class="col-12">
                <label class="form-label fs-4 text-black-50">Last Name</label>
                <input class="form-control fs-5 text-dark" type="text" name="lname" />
            </div>

            <br/><br/>


            <div class="col-12">
                <label class="form-label fs-4 text-black-50">Mobile</label>
                <input class="form-control fs-5 text-dark" type="text" name="mobile" />
            </div>

            <br/><br/>



            <div class="col-12">
                <label class="form-label fs-4 text-black-50">Password</label>
                <input class="form-control fs-5 text-dark" type="password" name="password" />
            </div>


            <br/><br/>

            <div class="col-12">
                <label class="form-label fs-4 text-black-50">Gender</label>
                <input class="form-control fs-5 text-dark" type="text" name="gender" />
            </div>

            <br/><br/>
            <div class="col-12 d-flex justify-content-center align-content-center">
            <input class="btn btn-outline-warning fs-3" type="submit" value="Sign in"/>
            </div>

        </form>
        </div>
    </div>
</div>

</body>
</html>
