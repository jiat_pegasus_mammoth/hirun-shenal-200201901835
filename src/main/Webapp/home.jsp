<%@ page import="java.sql.Connection" %>
<%@ page import="com.jiat.db.DBConnection" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %><%--
  Created by IntelliJ IDEA.
  User: ASUS TUF
  Date: 3/31/2023
  Time: 3:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>

<div class="container-fluid ">
    <div class="row align-content-center ">

        <div class="col-6 offset-3 bg-white">

            <h1 class="text-center h3-class fs-1 mt-5">Users</h1>

            <table class="table table-info table-striped table-hover mt-3">

                <tr>
                    <th> User ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th> Mobile Number</th>
                    <th> Password</th>
                    <th> Gender</th>
                </tr>

                <%

                    Connection connection = null;

                    try {

                        connection = DBConnection.getConnection();

                        ResultSet result = DBConnection.search("SELECT * FROM `user`");

                        while (result.next()) {
                %>
                <tr>
                    <td><%= result.getString("id") %>
                    </td>
                    <td><%= result.getString("fname") %>
                    </td>
                    <td><%= result.getString("lname") %>
                    </td>
                    <td><%= result.getString("mobile") %>
                    </td>
                    <td><%= result.getString("password") %>
                    </td>
                    <td><%= result.getString("gender") %>
                    </td>
                </tr>
                <%
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    } finally {

                        if (connection != null) {

                            try {
                                connection.close();
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }

                        }

                    }

                %>

            </table>

        </div>
    </div>
</div>

</body>
</html>
