package com.jiat;

import com.jiat.db.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "Login", urlPatterns = "/login")

public class Login extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String mobile = request.getParameter("mobile");
        String password = request.getParameter("password");
        String gender = request.getParameter("gender");

        Connection connection = null;

        try {

            connection = DBConnection.getConnection();

            DBConnection.iud("INSERT INTO `user`(`fname`,`lname`,`mobile`,`password`,`gender`) VALUES ('" + fname + "','" + lname + "','" + mobile + "','" + password + "','" + gender + "')");

          response.sendRedirect("home.jsp");

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (connection != null) {

                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }

        }
    }
}
